Day 1. Assignment

1. MCQ
----------------------------------------------------------------------------------------------------------
	Question 1: What does HTML stand for?
	A) Home Tool Markup Language
	B) Hyperlinks and Text Markup Language
	C) Hyper Text Markup Language      
	
	Question 2: Who is making the Web standards?
	A) Microsoft
	B) Mozilla
	C) The World Wide Web Consortium      
	D) Google
	
	Question 3: Choose the correct HTML element for the largest heading:
	A) <h1>	     B) <head>	C) <heading>	D) <h6>
	
	Question 4: What is the correct HTML element for inserting a line break?
	A) <br>      B) <lb>	C) <break>
	
	Question 5: What is the correct HTML for adding a background color?
	A) <background>yellow</background>
	B) <body style="background-color:yellow;">      
	D) <body bg="yellow">
	
	Question 6: Choose the correct HTML element to define important text
	A) <b>	B) <strong>      C) <important>	D) <i>
	
	Question 7: What is the correct HTML for creating a hyperlink?
	A) <a href="http://www.w3schools.com">W3Schools</a>      
	B) <a>http://www.w3schools.com</a>
	C) <a url="http://www.w3schools.com">W3Schools.com</a>
	D) <a name="http://www.w3schools.com">W3Schools.com</a>
	
	Question 8: Which character is used to indicate an end tag?
	A) /      B) *	C) <	D) ^
	
	Question 9: How can you open a link in a new tab/browser window?
	A) <a href="url" new>
	B) <a href="url" target="new">
	C) <a href="url" target="_blank">      
	
	Question 10: Which of these elements are all <table> elements?
	A) <table><tr><td>      
	B) <table><tr><tt>
	C) <table><head><tfoot>
	D) <thead><body><tr>
	
	Question 11: How can you make a numbered list?
	A) <list>
	B) <dl>
	C) <ol>      
	D) <ul>
	
	Question 12: How can you make a bulleted list?
	A) <ol>	B) <dl>	C) <ul>      D) <list>
	
	Question 13: What is the correct HTML for making a checkbox?
	A) <input type="checkbox">      
	B) <checkbox>
	C) <input type="check">
	D) <check>
	
	Question 14: What is the correct HTML for making a text input field?
	A) <input type="textfield">
	B) <textinput type="text">
	C) <textfield>
	D) <input type="text">      
	
	Question 15: What is the correct HTML for making a drop-down list?
	A) <list>
	B) <input type="list">
	C) <input type="dropdown">
	D) <select>


2. Create a web page which will have:
-------------------------------------
1. All header tags
2. List of fruits
3. show image using relative path
4. table showing list of students ( shoud have caption/thead/tbody/tfoot tags )
5. create table as shown in image




