<?php

	header('Access-Control-Allow-Origin: *');  

	$num = $_REQUEST["num"];
	
	$result = [ 
				"square" => $num * $num, 
				"cube" => $num * $num * $num
			];
	
	echo json_encode($result);
	
?>